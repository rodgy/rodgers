package com.example.root.canvasapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Button one;
    private Button two;
    private Button three;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        one = findViewById(R.id.screen1);
        two = findViewById(R.id.screen2);
        three = findViewById(R.id.screen3);

        one.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);


    }

    private void screens(){
        Intent intent =  new Intent(MainActivity.this, Screen1.class);
        startActivity(intent);
    }

    private void screens2(){
        Intent intent =  new Intent(MainActivity.this, Screen2.class);
        startActivity(intent);
    }

    private void screens3(){
        Intent intent =  new Intent(MainActivity.this, Screen3.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v){
        int id = v.getId();

        if(id == R.id.screen1){
            screens();
        }
        if(id == R.id.screen2){
            screens2();
        }
        if(id == R.id.screen3){
            screens3();
        }
    }
}
